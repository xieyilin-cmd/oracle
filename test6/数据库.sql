

-- 创建表空间 

CREATE TEMPORARY TABLESPACE Goodsale_TEMP
         TEMPFILE 'E:/oracle_tabledatabase/Goodsale_TEMP.DBF'
         SIZE 32M
         AUTOEXTEND ON
         EXTENT MANAGEMENT LOCAL;
     

CREATE TABLESPACE Goodsale
         LOGGING
         DATAFILE 'E:/oracle_tabledatabase/Goodsale.DBF'
         SIZE 32M
         AUTOEXTEND ON
         EXTENT MANAGEMENT LOCAL;


-- 创建管理员并授权
CREATE USER Goodsale_admin IDENTIFIED BY oracle ACCOUNT UNLOCK DEFAULT TABLESPACE Goodsale TEMPORARY TABLESPACE Goodsale_TEMP;
GRANT CONNECT,RESOURCE TO Goodsale_admin; 
GRANT DBA TO Goodsale_admin;

-- 创建普通用户并授权
CREATE USER Goodsale_user IDENTIFIED BY oracle ACCOUNT UNLOCK DEFAULT TABLESPACE Goodsale TEMPORARY TABLESPACE Goodsale_TEMP;
GRANT CONNECT,RESOURCE TO Goodsale_user; 


-- 连接数据库
connect Goodsale_admin/oracle;

-- 创建表和模拟数据

-- Customer顾客信息表
create table Customers(
Customerid  VARCHAR(20) not null primary key,
CustomerName  VARCHAR(20),
CustomerSex VARCHAR(2),
CustomerBirth date,
CustomerPhone VARCHAR(50)
);

-- goods商品信息表
create table goods(
GoodId VARCHAR(20)  not null primary key,
GoodName VARCHAR(50) not null,
GoodPrice float not null,
GoodType VARCHAR(20),
GoodStock int,
GoodState VARCHAR(20)
);


-- orders订单信息表
create table orders(
orderid int PRIMARY KEY,
CustomerId  VARCHAR(20),
OrderDate  date,  
OrderStatue  VARCHAR(20),
foreign key(Customerid) references Customers(Customerid)
);
-- 订单明细表（order_details）
create table orderdetails (
orderid int NOT NULL,
GoodId VARCHAR(20),
OrderNum  int   not null,
OrderPrice  float  not null,
primary key (orderid, GoodId),
foreign key (orderid) references orders(orderid),
foreign key (GoodId) references goods(GoodId)
);

set serveroutput on
-- 创建存储过程,每个表模拟5万条数据
CREATE OR REPLACE PROCEDURE InsertMockData AS
BEGIN
  -- 插入顾客信息
  FOR i IN 1..50000 LOOP
    INSERT INTO Customers (Customerid, CustomerName, CustomerSex, CustomerBirth, CustomerPhone)
    VALUES ('c' || LPAD(i, 6, '0'), 'Customer' || i, CASE MOD(i, 2) WHEN 0 THEN '女' ELSE '男' END, DATE '2000-01-01', '1881234567' || LPAD(i, 3, '0'));
  END LOOP;

  -- 插入商品信息
  FOR i IN 1..50000 LOOP
    INSERT INTO goods (GoodId, GoodName, GoodPrice, GoodType, GoodStock, GoodState)
    VALUES ('s' || LPAD(i, 6, '0'), 'Good' || i, 1000 + i, CASE MOD(i, 3) WHEN 0 THEN '家电' WHEN 1 THEN '数码' ELSE '手机' END, 100, '出售中');
  END LOOP;

  -- 插入订单信息和订单明细
  FOR i IN 1..50000 LOOP
    INSERT INTO orders (orderid, CustomerId, OrderDate, OrderStatue)
    VALUES (i, 'c' || LPAD(i, 6, '0'), DATE '2023-01-01', CASE MOD(i, 5) WHEN 0 THEN '成功' ELSE '取消' END);
    
    INSERT INTO orderdetails (orderid, GoodId, OrderNum, OrderPrice)
    VALUES (i, 's' || LPAD(i, 6, '0'), 1, 1000 + i);
  END LOOP;
  
  COMMIT;
  
  DBMS_OUTPUT.PUT_LINE('成功.');
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('失败: ' || SQLERRM);
    ROLLBACK;
END;
/
BEGIN
  insertmockdata;
END;
/
select count(*) from customers;
select count(*) from goods;
select count(*) from orders;
select count(*) from orderdetails;
-- 创建程序包
CREATE OR REPLACE PACKAGE YourPackage AS
  -- 存储过程示例：根据顾客ID获取订单总数
  PROCEDURE GetOrderCountByCustomerId(p_customer_id IN VARCHAR2, p_order_count OUT NUMBER);

  -- 函数示例：计算订单总金额
  FUNCTION CalculateTotalOrderAmount(p_order_id IN NUMBER) RETURN NUMBER;
END YourPackage;
/

CREATE OR REPLACE PACKAGE BODY YourPackage AS
  -- 存储过程实现：根据顾客ID获取订单总数
  PROCEDURE GetOrderCountByCustomerId(p_customer_id IN VARCHAR2, p_order_count OUT NUMBER) AS
  BEGIN
    SELECT COUNT(*) INTO p_order_count
    FROM orders
    WHERE CustomerId = p_customer_id;
  END GetOrderCountByCustomerId;

  -- 函数实现：计算订单总金额
  FUNCTION CalculateTotalOrderAmount(p_order_id IN NUMBER) RETURN NUMBER AS
    v_total_amount NUMBER := 0;
  BEGIN
    SELECT SUM(OrderPrice) INTO v_total_amount
    FROM orderdetails
    WHERE orderid = p_order_id;
    
    RETURN v_total_amount;
  END CalculateTotalOrderAmount;
END YourPackage;
/

-- 调用程序包
DECLARE
  order_count NUMBER;
  total_amount NUMBER;
BEGIN
  -- 调用存储过程
  YourPackage.GetOrderCountByCustomerId('c000462', order_count);
  DBMS_OUTPUT.PUT_LINE('订单总数: ' || order_count);

  -- 调用函数
  total_amount := YourPackage.CalculateTotalOrderAmount(1);
  DBMS_OUTPUT.PUT_LINE('计算订单总金额: ' || total_amount);
END;
/

