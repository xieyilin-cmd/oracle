﻿﻿<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

##  创建表空间 

```sql
CREATE TEMPORARY TABLESPACE Goodsale_TEMP

    TEMPFILE 'E:/oracle_tabledatabase/Goodsale_TEMP.DBF'

    SIZE 32M

    AUTOEXTEND ON

    EXTENT MANAGEMENT LOCAL;

    

CREATE TABLESPACE Goodsale
   LOGGING

   DATAFILE 'E:/oracle_tabledatabase/Goodsale.DBF'

   SIZE 32M

   AUTOEXTEND ON

   EXTENT MANAGEMENT LOCAL;
```

 

![img](wps1.jpg) 

## 创建用户并授权：

### 1.创建管理员并授权

```sql
CREATE USER Goodsale_admin IDENTIFIED BY oracle ACCOUNT UNLOCK DEFAULT TABLESPACE Goodsale TEMPORARY TABLESPACE Goodsale_TEMP;

GRANT CONNECT,RESOURCE TO Goodsale_admin; 

GRANT DBA TO Goodsale_admin;
```

![img](wps2.jpg) 

### 1.创建普通用户并授权

```sql
CREATE USER Goodsale_user IDENTIFIED BY oracle ACCOUNT UNLOCK DEFAULT TABLESPACE Goodsale TEMPORARY TABLESPACE Goodsale_TEMP;

GRANT CONNECT,RESOURCE TO Goodsale_user; 
```

![img](wps3.jpg) 

 

## 创建表

###  1.Customer顾客信息表

```sql
create table Customers(

Customerid  VARCHAR(10) not null primary key,

CustomerName  VARCHAR(10),

CustomerSex VARCHAR(2),

CustomerBirth date,

CustomerPhone VARCHAR(50)

);
```

![img](wps4.jpg) 

### 2.goods商品信息表

```sql
create table goods(

GoodId VARCHAR(10)  not null primary key,

GoodName VARCHAR(50) not null,

GoodPrice float not null,

GoodType VARCHAR(10),

GoodStock int,

GoodState VARCHAR(10)

);
```

![img](wps5.jpg) 

 

### 3.orders订单信息表

```sql
create table orders(

orderid int PRIMARY KEY,

CustomerId  VARCHAR(10),

GoodId   VARCHAR(10),

OrderDate  date,  

OrderStatue  VARCHAR(20),

foreign key(Customerid) references Customers(Customerid),

foreign key(GoodId) references goods(GoodId)

);
```

![img](wps6.jpg) 

### 订单明细表（order_details）

```sql
create table orderdetails (

orderid int NOT NULL,

GoodId VARCHAR(10),

OrderNum  int  not null,

OrderPrice  float  not null,

primary key (orderid, GoodId),

foreign key (orderid) references orders(orderid),

foreign key (GoodId) references goods(GoodId)

);
```

![img](wps7.jpg) 

 

## 创建存储过程,每个表模拟5万条数据

```sql
CREATE OR REPLACE PROCEDURE InsertMockData AS

BEGIN

 -- 插入顾客信息

 FOR i IN 1..50000 LOOP

  INSERT INTO Customers (Customerid, CustomerName, CustomerSex, CustomerBirth, CustomerPhone)

  VALUES ('c' || LPAD(i, 6, '0'), 'Customer' || i, CASE MOD(i, 2) WHEN 0 THEN '女' ELSE '男' END, DATE '2000-01-01', '1881234567' || LPAD(i, 3, '0'));

 END LOOP;
 

 -- 插入商品信息

 FOR i IN 1..50000 LOOP

  INSERT INTO goods (GoodId, GoodName, GoodPrice, GoodType, GoodStock, GoodState)

  VALUES ('s' || LPAD(i, 6, '0'), 'Good' || i, 1000 + i, CASE MOD(i, 3) WHEN 0 THEN '家电' WHEN 1 THEN '数码' ELSE '手机' END, 100, '出售中');

 END LOOP;

 
 -- 插入订单信息和订单明细

 FOR i IN 1..50000 LOOP

  INSERT INTO orders (orderid, CustomerId, OrderDate, OrderStatue)

  VALUES (i, 'c' || LPAD(i, 6, '0'), DATE '2023-01-01', CASE MOD(i, 5) WHEN 0 THEN '成功' ELSE '取消' END);

  
  INSERT INTO orderdetails (orderid, GoodId, OrderNum, OrderPrice)

  VALUES (i, 's' || LPAD(i, 6, '0'), 1, 1000 + i);

 END LOOP;


 COMMIT;
 

 DBMS_OUTPUT.PUT_LINE('成功.');

EXCEPTION

 WHEN OTHERS THEN

  DBMS_OUTPUT.PUT_LINE('失败: ' || SQLERRM);

  ROLLBACK;

END;

BEGIN

 insertmockdata;

END;

```

![img](wps8.jpg) 

## 调用存储过程：

```sql
BEGIN

 GenerateTestData;

END;
```



![img](wps9.jpg) 

![img](wps10.jpg) 

 

## 创建程序包

```sql
CREATE OR REPLACE PACKAGE YourPackage AS

 -- 存储过程示例：根据顾客ID获取订单总数

 PROCEDURE GetOrderCountByCustomerId(p_customer_id IN VARCHAR2, p_order_count OUT NUMBER);


 -- 函数示例：计算订单总金额

 FUNCTION CalculateTotalOrderAmount(p_order_id IN NUMBER) RETURN NUMBER;

END YourPackage;



CREATE OR REPLACE PACKAGE BODY YourPackage AS

 -- 存储过程实现：根据顾客ID获取订单总数

 PROCEDURE GetOrderCountByCustomerId(p_customer_id IN VARCHAR2, p_order_count OUT NUMBER) AS

 BEGIN

  SELECT COUNT(*) INTO p_order_count

  FROM orders

  WHERE CustomerId = p_customer_id;

 END GetOrderCountByCustomerId;

 
 -- 函数实现：计算订单总金额

 FUNCTION CalculateTotalOrderAmount(p_order_id IN NUMBER) RETURN NUMBER AS

  v_total_amount NUMBER := 0;

 BEGIN

  SELECT SUM(OrderPrice) INTO v_total_amount

  FROM orderdetails

  WHERE orderid = p_order_id;


  RETURN v_total_amount;

 END CalculateTotalOrderAmount;

END YourPackage;

```

![img](wps11.jpg) 

## 调用程序包

```sql
DECLARE

 order_count NUMBER;

 total_amount NUMBER;

BEGIN

 -- 调用存储过程

 YourPackage.GetOrderCountByCustomerId('c000462', order_count);

 DBMS_OUTPUT.PUT_LINE('订单总数: ' || order_count);

 
 -- 调用函数

 total_amount := YourPackage.CalculateTotalOrderAmount(1);

 DBMS_OUTPUT.PUT_LINE('计算订单总金额: ' || total_amount);

END;

```

![img](wps12.jpg) 

 

## 备份方案：

### 定义备份策略：

1.确定备份的频率，例如每日、每周或每月。

2.确定完整备份和增量备份的结合方式。

3.确定备份保留周期，即备份文件的保留时间。

### 定义备份类型：

完整备份：备份整个数据库的所有数据和对象。

增量备份：备份自上次完整备份以来的增量更改。

### 定义备份媒介：

确定备份文件存储位置，可以是本地磁盘、网络共享、云存储等。

考虑备份文件的冗余性，可以在多个位置存储备份以提高可靠性。

### 定义备份计划：

创建自动化备份脚本或作业，以确保备份按计划进行。

考虑备份时间窗口，选择数据库负载较低的时段进行备份。

### 测试和验证备份：

 

定期还原备份以验证备份数据的完整性和可恢复性。

监控备份过程，确保备份成功并记录备份日志。

### 灾难恢复计划：

 

定义灾难恢复计划，包括从备份恢复数据的步骤和流程。

确定关键数据和系统的优先级，以便在灾难发生时进行恢复。

### 定期评估和更新备份方案：

定期评估备份方案的有效性和性能。

根据数据库的变化和需求进行调整和更新备份方案
##实验总结
在这个期末考核实验中，我完成了基于Oracle数据库的商品销售系统的设计。以下是我对该实验的总结：

数据库设计方案：

表及表空间设计方案：我设计了四张表，分别是顾客信息表（Customers）、商品信息表（goods）、订单信息表（orders）和订单明细表（orderdetails）。我使用了两个表空间，一个用于数据文件（Goodsale），另一个用于临时文件（Goodsale_TEMP）。
模拟数据：我使用存储过程生成了10万条模拟数据，包括顾客信息、商品信息、订单信息和订单明细。
用户管理：

管理员用户：我创建了一个管理员用户（Goodsale_admin），并给予了CONNECT、RESOURCE和DBA角色的授权。
普通用户：我创建了一个普通用户（Goodsale_user），并给予了CONNECT和RESOURCE角色的授权。
存储过程和函数设计：

我在数据库中创建了一个程序包（YourPackage），其中包含了两个存储过程和一个函数。
存储过程：GetOrderCountByCustomerId根据顾客ID获取订单总数；GenerateTestData用于生成模拟数据。
函数：CalculateTotalOrderAmount计算指定订单的总金额。
备份方案：

我设计了一个备份策略，包括备份的频率、备份类型和备份保留周期。
备份类型包括完整备份和增量备份。
我选择了合适的备份媒介，并确定了备份文件存储的位置。
我创建了自动化备份脚本或作业，确保备份按计划进行。
我定期测试和验证备份数据的完整性和可恢复性，并监控备份过程。
通过完成这个实验，我深入了解了Oracle数据库的设计和管理，学会了创建表空间、用户和表，编写存储过程和函数，并设计了合适的备份方案。这个实验对我加深了对数据库管理的理解，并提高了我的实践能力和问题解决能力。