# 实验4：PL/SQL语言打印杨辉三角

姓名：谢易霖

学号：202010414421

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriangle，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriangle的SQL语句。

## 杨辉三角源代码

```sql
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/
```

![picture1](pict1.png)



## 实验总结

在使用PL/SQL语言打印杨辉三角的实验中，我总结如下：

1. 杨辉三角的生成：杨辉三角是一个数字三角形，每个位置的数字是上方两个数字之和。使用PL/SQL语言可以通过循环和数组来生成杨辉三角。在实验中，可以使用嵌套循环和数组来迭代计算每个位置的数字，并将其存储在相应的数组元素中。
2. 数组的使用：PL/SQL语言提供了数组的支持，可以用来存储杨辉三角中的数字。在实验中，可以声明一个二维数组来表示杨辉三角，其中每一行表示一个分层，每一列表示该分层中的数字。通过循环和数组索引，可以计算并存储每个位置的数字。
3. 输出结果：在实验中，可以使用DBMS_OUTPUT.PUT_LINE函数来输出杨辉三角的结果。可以通过循环遍历数组，并使用该函数将每个位置的数字打印出来。另外，可以在输出时对数字进行格式化，以便更好地展示杨辉三角的结构。
4. 控制结构：在实验中，可以使用条件语句和循环语句来控制杨辉三角的生成和输出过程。可以使用FOR循环来遍历每一层，使用IF条件语句来判断边界条件和计算规则，以确保正确生成杨辉三角。
5. 扩展功能：除了打印基本的杨辉三角，还可以通过扩展实验来实现其他功能。例如，可以编写一个函数来计算指定位置的数字，或者编写一个过程来生成指定行数的杨辉三角。

总结而言，通过使用PL/SQL语言，我们可以灵活地生成和打印杨辉三角。利用循环、数组、控制结构和输出函数等特性，可以实现杨辉三角的计算和展示。这样的实验有助于加深对PL/SQL语言的理解和应用，并提升编程能力。